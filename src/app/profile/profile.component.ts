import { Component } from '@angular/core';
import { User, UserService } from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.css'
})
export class ProfileComponent {

  profile: User | null = null;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.profile = this.userService.getCurrentUser();
  }
}
