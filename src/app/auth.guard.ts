import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { UserService } from './user.service';

export const authGuard: CanActivateFn = (route, state) => {
  const isAuthenticated = inject(UserService).isAuthenticated();

  if (isAuthenticated) {
    return true;
  } else {
    return inject(Router).navigate(['/home']);
  }
};
