import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export interface User {
  id: number;
  username: string;
  password: string;
  isAuthenticated: boolean;
  firstName?: string;
  lastName?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = [
    {
      id: 1,
      username: 'homer',
      password: '12345',
      isAuthenticated: false,
      firstName: 'Homer',
      lastName: 'Simpson'
    }
  ];

  currentUser: User | null = null;

  constructor(private router: Router) { }

  login(username: string, password: string): void {
    let user = this.users.find(user => user.username === username);

    if (user && user.password === password) {
      user.isAuthenticated = true;
      this.currentUser = user;
      this.router.navigate(['/profile']);
    }
  }

  logout(username: string): void {
    let user = this.users.find(user => user.username === username)

    if (user) {
      user.isAuthenticated = false;
      this.currentUser = null;
      this.router.navigate(['/home']);
    }
  }

  isAuthenticated(): boolean {
    return this.currentUser?.isAuthenticated ?? false;
  }

  getCurrentUser(): User | null {
    return this.currentUser;
  }
}