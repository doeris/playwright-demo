import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  username: string = '';
  password: string = '';

  isLoading: boolean = false;

  constructor(private userService: UserService) { }

  onSubmit(): void {
    this.isLoading = true;

    setTimeout(() => {
      this.userService.login(this.username, this.password);
      this.isLoading = false;
    }, 2000);
  }
}
