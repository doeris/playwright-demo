import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  loginDisplay = false;
  changeDetected = false;

  constructor(private userService: UserService) { }

  ngDoCheck() {
    if (this.userService.isAuthenticated() !== this.loginDisplay) {
      this.changeDetected = true;
      this.loginDisplay = this.userService.isAuthenticated();
    }

    this.changeDetected = false;
  }
}
