import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'playwright demo';
  loginDisplay = false;
  changeDetected = false;

  constructor(private userService: UserService) { }

  ngDoCheck() {
    if (this.userService.isAuthenticated() !== this.loginDisplay) {
      this.changeDetected = true;
      this.loginDisplay = this.userService.isAuthenticated();
    }

    this.changeDetected = false;
  }

  logout(): void {
    this.loginDisplay = false;
    this.userService.logout(this.userService.getCurrentUser()?.username ?? '');
  }
}
