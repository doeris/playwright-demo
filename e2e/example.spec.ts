import { test, expect } from '@playwright/test';

test.beforeAll(async () => {
  // actions to perform once before everything else runs
});

test.beforeEach(async () => {
  // actions to perform before each test
});

test.afterAll(async () => {
  // actions to perform once after all tests are done
});

test.afterEach(async () => {
  // actions to perform after each test
});

test('verify that login form works', async ({ page }) => {
  await page.goto('http://localhost:4200/');
  await page.goto('http://localhost:4200/home');
  await page.getByRole('button', { name: 'Login' }).click();
  await page.getByLabel('Username').click();
  await page.getByLabel('Username').fill('homer');
  await page.getByLabel('Username').press('Tab');
  await page.getByLabel('Password').fill('12345');
  await page.locator('mat-card-actions').getByRole('button', { name: 'Login' }).click();
  await expect(page.getByRole('img', { name: 'User Avatar' })).toBeVisible();
});
