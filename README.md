# Playwright Demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Playwright](https://playwright.dev/).

Use `npm run e2e:ui` to work with a UI that shows detailed test analysis.

Use `npm run e2e:codegen` to capture your actions and generate test cases. (p.s. because I'm being lazy at the moment, you need to comment out the `webServer` field in `playwright.config.ts` and run the project via `npm start` (in a separate terminal window) first before running `npm run e2e:codegen`)

## Further help

- [Playwright Documentation](https://playwright.dev/docs/intro)
